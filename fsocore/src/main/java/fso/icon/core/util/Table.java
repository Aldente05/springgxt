package fso.icon.core.util;

/**
 * Created by kris on 15/05/14.
 * Provide utility for DML,DDL
 */
public class Table {

    /**
     * All table name need separate with space at the end of sentence
     */

    public static final String KEUANGAN_KAS = "keuangan_kas ";
    public static final String KEUANGAN_HUTANG_REKANAN = "keuangan_hutang_rekanan ";
    public static final String KEUANGAN_REKENING = "keuangan_rekening ";
    public static final String KEUANGAN_RUGI_LABA = "keuangan_rugi_laba ";
    public static final String KEUANGAN_SELISIH = "keuangan_selisih ";
    public static final String KEUANGAN_PENDING_CARD = "keuangan_pending_card ";
    public static final String KEUANGAN_PENGELUARAN_BIAYA = "keuangan_pengeluaran_biaya";
    public static final String KEUANGAN_TRANSAKSI_PENERIMAAN_SETORAN = "keuangan_transaksi_penerimaan_setoran";
    public static final String KEUANGAN_TRANSAKSI_DANA = "keuangan_transaksi_dana";

    public static final String MASTER_AGAMA = "master_agama ";
    public static final String MASTER_BANK = "master_bank ";
    public static final String MASTER_BARANG = "master_barang ";
    public static final String MASTER_JENIS_BARANG = "master_jenis_barang ";
    public static final String MASTER_MENU = "master_menu ";
    public static final String MASTER_MENU_DETAIL = "master_menu_detail ";
    public static final String MASTER_PENGELUARAN = "master_pengeluaran ";
    public static final String MASTER_SATUAN_BARANG = "master_satuan_barang ";
    public static final String MASTER_SATUAN_MENU = "master_satuan_menu ";
    public static final String MASTER_SUPPLIER = "master_supplier ";

    public static final String PENERIMAAN_BARANG = "penerimaan_barang ";
    public static final String PENERIMAAN_BARANG_DETAIL = "penerimaan_barang_detail ";
    public static final String PENGELUARAN_BARANG = "pengeluaran_barang ";
    public static final String PENGELUARAN_BARANG_DETAIL = "pengeluaran_barang_detail ";
    public static final String PENGADAAN_BARANG = "pengadaan_barang ";
    public static final String PENGADAAN_BARANG_DETAIL = "pengadaan_barang_detail ";
    public static final String PENJUALAN_MENU = "penjualan_menu ";
    public static final String PENJUALAN_MENU_DETAIL = "penjualan_menu_detail ";
    public static final String REKAP_PENJUALAN_MENU = "rekap_penjualan_menu ";
    public static final String REKAP_PENGADAAN = "rekap_pengadaan ";
    public static final String RETUR_BARANG = "retur_barang ";
    public static final String RETUR_BARANG_DETAIL = "retur_barang_detail ";
    public static final String STOK_BARANG = "stok_barang ";

    public static final String OAUTH_CLIENT_DETAILS = "oauth_client_details ";
    public static final String OAUTH_ACCESS_TOKEN = "oauth_access_token ";

    public static final String SETTING_HARGA_BARANG = "setting_harga_barang ";
    public static final String SETTING_PAKET = "setting_paket ";
    public static final String SETTING_PAKET_DETAIL = "setting_paket_detail ";
    public static final String SETTING_PROMO = "setting_promo ";
    public static final String SETTING_PROMO_DETAIL = "setting_promo_detail ";
    public static final String SYSTEM_MENU = "system_menu ";
    public static final String SYSTEM_ROLE = "system_role ";
    public static final String SYSTEM_USERS = "system_users ";
    public static final String SYSTEM_PARAMETER = "system_parameter ";
    public static final String SYSTEM_ROLE_USER = "system_role_user ";
    public static final String SYSTEM_ROLE_MENU = "system_role_menu ";

    public static final String TRANSAKSI_BARANG = "transaksi_barang ";
    public static final String TRANSAKSI_BILLING = "transaksi_billing ";
    public static final String TRANSAKSI_BILLING_DETAIL = "transaksi_billing_detail ";
    public static final String MESSAGE_UNKNOWN = "message_unknown ";

    public static final String LOG_USER = "log_user ";


}
