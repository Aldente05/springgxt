package fso.icon.core.util;

/**
 * Created by f.putra on 7/20/17.
 * package and function PLSQL
 * for module core
 */
public class OracleFunction {

    /**
     * package oracle PLQL
     * static param for package
     * use this on service for implement / override interface data
     */
    private static String PKG_SECMAN = "pkg_secman";
    private static String PKG_MASTER_FSO = "pkg_master_fso";
    private static String PKG_TRANSAKSI = "pkg_transaksi";
    private static String PKG_MASTER = "pkg_master";

    /**
     * function oracle PLSQL
     * static param for function
     * use this on service implement method
     */
    private static String INSERT_MASTER_VENDOR = "insert_master_vendor";
    private static String INSERT_MASTER_PETUGAS = "insert_master_petugas";
    private static String INSERT_MASTER_HARILIBUR = "ins_master_harilibur";
    private static String INSERT_KIRIM_SO_KEVENDOR_TMP = "ins_kirim_so_kevendor_tmp";
    private static String INSERT_KIRIM_SO_KEVENDOR = "ins_kirim_so_kevendor";

    private static String GET_LIST_MENU_BY_ID = "get_list_menu_byid";
    private static String GET_GRID_MASTER_VENDOR = "getGridMasterVendor";
    private static String GET_GRID_MASTER_PETUGAS = "getGridMasterPetugas";
    private static String GET_LIST_MASTER_HARILIBUR = "get_list_master_harilibur";
    private static String GET_LIST_REGION = "get_list_region";
    private static String GET_LIST_PENUGASAN_SO = "get_list_penugasan_so";
    private static String GET_CB_VENDOR = "get_cb_vendor";
    private static String GET_LIST_VERIFIKASI_SO = "get_list_verifikasi_so";
    private static String GET_DETIL_VERIFIKASI_SO = "get_detil_verifikasi_so";

    private static String PTL_VERIFIKASI_SO = "ptl_verifikasi_so";
    private static String PTL_KIRIM_MATERIAL_SURVEI = "ptl_kirim_material_survei";

}
