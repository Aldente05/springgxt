package fso.icon.core.service.impl;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import fso.icon.core.entity.Menu;
import fso.icon.core.entity.User;
import fso.icon.core.service.NavigationMenuService;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by f.putra on 7/20/17.
 */
@Service
public class NavigationMenuImpl implements NavigationMenuService {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Menu> menuByUser(User user) {
        ObjectMapper objectMapper = new ObjectMapper();
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName("PKG_SECMAN")
                .withFunctionName("get_list_menu_byid");

        SqlParameterSource in = new MapSqlParameterSource().addValue("p_id_user", user.getEmail());
        Map<String, Object> m = new HashMap<>(jdbcCall.execute(in));
        JsonElement jsonElement = new Gson().toJsonTree(m);
        try {
            List<Menu> resultFinal = objectMapper.readValue(jsonElement.getAsJsonObject().get("return").toString(), objectMapper.getTypeFactory().constructCollectionType(List.class, Menu.class));
            return resultFinal;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Menu> createTreeMenu(User role) {
        List<Menu> menus = new ArrayList<>();
        List<Menu> currentMenus = menuByUser(role);
        for (Menu menu : currentMenus) {
            if (menu.getIdParent() == 0) {
                menus.add(menu);
                menus.addAll(hasChild(menu, currentMenus, new ArrayList<Menu>()));
            }
        }
        return menus;
    }

    private List<Menu> hasChild(Menu akun, List<Menu> menus, List<Menu> callback) {
        for (Menu menu : menus) {
            if (menu.getIdParent().equals(akun.getId())) {
                callback.add(menu);
                hasChild(menu, menus, callback);
            }
        }
        return callback;
    }
}
