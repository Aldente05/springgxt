package fso.icon.core.service;

import fso.icon.core.entity.Menu;
import fso.icon.core.entity.User;

import java.util.List;

/**
 * Created by f.putra on 7/20/17.
 */
public interface NavigationMenuService {


    List<Menu> menuByUser (User user);

    List<Menu> createTreeMenu (User role);
}
