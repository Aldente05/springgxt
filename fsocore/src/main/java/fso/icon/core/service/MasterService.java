package fso.icon.core.service;

import fso.icon.core.entity.Vendor;

import java.util.List;
import java.util.Map;

/**
 * Created by f.putra on 7/27/17.
 */
public interface MasterService {


    Map<String, Object> insertMasterVendor();
    Map<String, Object> insertMasterPetugas();
    Map<String, Object> insertMasterHariLibur();

    List<Vendor> getDataVendor();
}
