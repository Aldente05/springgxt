package fso.icon.core.entity;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by f.putra on 7/20/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Menu implements Serializable {

    private static final long serialVersionUID = -4176398666394762234L;
    @JsonProperty("ID_MENU")
    private Integer id;
    @JsonProperty("ROOT_ID")
    private Integer idParent;
    @JsonProperty("TEXT")
    private String title;
    @JsonProperty("FORM_ID")
    private String routes;
    @JsonProperty("ICON_STYLE")
    private String icon;
    @JsonProperty("MENU_ORDER")
    private String orderNumber;

    private List<Menu> childern ;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdParent() {
        return idParent;
    }

    public void setIdParent(Integer idParent) {
        this.idParent = idParent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRoutes() {
        return routes;
    }

    public void setRoutes(String routes) {
        this.routes = routes;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public List<Menu> getChildern() {
        return childern;
    }

    public void setChildern(List<Menu> childern) {
        this.childern = childern;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\"=" + id +
                ", \"idParent\"=" + idParent +
                ", \"title\"='" + title + '\'' +
                ", \"routes\"='" + routes + '\'' +
                ", \"icon\"='" + icon + '\'' +
                ", \"orderNumber\"='" + orderNumber + '\'' +
                ", \"childern\"=" + childern +
                '}';
    }
}
