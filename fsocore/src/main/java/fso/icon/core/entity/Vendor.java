package fso.icon.core.entity;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;

/**
 * Created by f.putra on 7/27/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Vendor implements Serializable{

    private int is_insert;
    @JsonProperty("KODE_VENDOR")
    private String KODE_VENDOR;
    @JsonProperty("NAMA_VENDOR")
    private String NAMA_VENDOR;
    @JsonProperty("JENIS_VENDOR")
    private String JENIS_VENDOR;
    @JsonProperty("ALAMAT_VENDOR")
    private String ALAMAT_VENDOR;
    @JsonProperty("TELP_VENDOR")
    private String TELP_VENDOR;
    @JsonProperty("EMAIL_VENDOR")
    private String EMAIL_VENDOR;
    @JsonProperty("PIC_VENDOR")
    private String PIC_VENDOR;
    @JsonProperty("TELP_PIC_VENDOR")
    private String TELP_PIC_VENDOR;
    @JsonProperty("HP_PIC_VENDOR")
    private String HP_PIC_VENDOR;
    @JsonProperty("KD_PIC_VENDOR")
    private String KD_PIC_VENDOR;
    @JsonProperty("UNITUPI")
    private String UNITUPI;
    @JsonProperty("UNITAP")
    private String UNITAP;
    @JsonProperty("UNITUP")
    private String UNITUP;
    @JsonProperty("ISAKTIF")
    private String ISAKTIF;

    public Vendor(int is_insert, String KODE_VENDOR, String NAMA_VENDOR, String JENIS_VENDOR, String ALAMAT_VENDOR, String TELP_VENDOR, String EMAIL_VENDOR, String PIC_VENDOR, String TELP_PIC_VENDOR, String HP_PIC_VENDOR, String KD_PIC_VENDOR, String UNITUPI, String UNITAP, String UNITUP, String ISAKTIF) {
        this.is_insert = is_insert;
        this.KODE_VENDOR = KODE_VENDOR;
        this.NAMA_VENDOR = NAMA_VENDOR;
        this.JENIS_VENDOR = JENIS_VENDOR;
        this.ALAMAT_VENDOR = ALAMAT_VENDOR;
        this.TELP_VENDOR = TELP_VENDOR;
        this.EMAIL_VENDOR = EMAIL_VENDOR;
        this.PIC_VENDOR = PIC_VENDOR;
        this.TELP_PIC_VENDOR = TELP_PIC_VENDOR;
        this.HP_PIC_VENDOR = HP_PIC_VENDOR;
        this.KD_PIC_VENDOR = KD_PIC_VENDOR;
        this.UNITUPI = UNITUPI;
        this.UNITAP = UNITAP;
        this.UNITUP = UNITUP;
        this.ISAKTIF = ISAKTIF;
    }

    public int getIs_insert() {
        return is_insert;
    }

    public void setIs_insert(int is_insert) {
        this.is_insert = is_insert;
    }

    public String getKODE_VENDOR() {
        return KODE_VENDOR;
    }

    public void setKODE_VENDOR(String KODE_VENDOR) {
        this.KODE_VENDOR = KODE_VENDOR;
    }

    public String getNAMA_VENDOR() {
        return NAMA_VENDOR;
    }

    public void setNAMA_VENDOR(String NAMA_VENDOR) {
        this.NAMA_VENDOR = NAMA_VENDOR;
    }

    public String getJENIS_VENDOR() {
        return JENIS_VENDOR;
    }

    public void setJENIS_VENDOR(String JENIS_VENDOR) {
        this.JENIS_VENDOR = JENIS_VENDOR;
    }

    public String getALAMAT_VENDOR() {
        return ALAMAT_VENDOR;
    }

    public void setALAMAT_VENDOR(String ALAMAT_VENDOR) {
        this.ALAMAT_VENDOR = ALAMAT_VENDOR;
    }

    public String getTELP_VENDOR() {
        return TELP_VENDOR;
    }

    public void setTELP_VENDOR(String TELP_VENDOR) {
        this.TELP_VENDOR = TELP_VENDOR;
    }

    public String getEMAIL_VENDOR() {
        return EMAIL_VENDOR;
    }

    public void setEMAIL_VENDOR(String EMAIL_VENDOR) {
        this.EMAIL_VENDOR = EMAIL_VENDOR;
    }

    public String getPIC_VENDOR() {
        return PIC_VENDOR;
    }

    public void setPIC_VENDOR(String PIC_VENDOR) {
        this.PIC_VENDOR = PIC_VENDOR;
    }

    public String getTELP_PIC_VENDOR() {
        return TELP_PIC_VENDOR;
    }

    public void setTELP_PIC_VENDOR(String TELP_PIC_VENDOR) {
        this.TELP_PIC_VENDOR = TELP_PIC_VENDOR;
    }

    public String getHP_PIC_VENDOR() {
        return HP_PIC_VENDOR;
    }

    public void setHP_PIC_VENDOR(String HP_PIC_VENDOR) {
        this.HP_PIC_VENDOR = HP_PIC_VENDOR;
    }

    public String getKD_PIC_VENDOR() {
        return KD_PIC_VENDOR;
    }

    public void setKD_PIC_VENDOR(String KD_PIC_VENDOR) {
        this.KD_PIC_VENDOR = KD_PIC_VENDOR;
    }

    public String getUNITUPI() {
        return UNITUPI;
    }

    public void setUNITUPI(String UNITUPI) {
        this.UNITUPI = UNITUPI;
    }

    public String getUNITAP() {
        return UNITAP;
    }

    public void setUNITAP(String UNITAP) {
        this.UNITAP = UNITAP;
    }

    public String getUNITUP() {
        return UNITUP;
    }

    public void setUNITUP(String UNITUP) {
        this.UNITUP = UNITUP;
    }

    public String getISAKTIF() {
        return ISAKTIF;
    }

    public void setISAKTIF(String ISAKTIF) {
        this.ISAKTIF = ISAKTIF;
    }

    @Override
    public String toString() {
        return "Vendor{" +
                "is_insert=" + is_insert +
                ", KODE_VENDOR='" + KODE_VENDOR + '\'' +
                ", NAMA_VENDOR='" + NAMA_VENDOR + '\'' +
                ", JENIS_VENDOR='" + JENIS_VENDOR + '\'' +
                ", ALAMAT_VENDOR='" + ALAMAT_VENDOR + '\'' +
                ", TELP_VENDOR='" + TELP_VENDOR + '\'' +
                ", EMAIL_VENDOR='" + EMAIL_VENDOR + '\'' +
                ", PIC_VENDOR='" + PIC_VENDOR + '\'' +
                ", TELP_PIC_VENDOR='" + TELP_PIC_VENDOR + '\'' +
                ", HP_PIC_VENDOR='" + HP_PIC_VENDOR + '\'' +
                ", KD_PIC_VENDOR='" + KD_PIC_VENDOR + '\'' +
                ", UNITUPI='" + UNITUPI + '\'' +
                ", UNITAP='" + UNITAP + '\'' +
                ", UNITUP='" + UNITUP + '\'' +
                ", ISAKTIF='" + ISAKTIF + '\'' +
                '}';
    }
}
