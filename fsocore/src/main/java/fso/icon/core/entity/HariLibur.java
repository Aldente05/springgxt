package fso.icon.core.entity;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;

/**
 * Created by f.putra on 7/27/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class HariLibur implements Serializable {

    @JsonProperty("P_KODE")
    private String p_kode;
    @JsonProperty("p_tgl_libur")
    private String p_tgl_libur;
    @JsonProperty("p_keterangan")
    private String p_keterangan;
    @JsonProperty("p_is_aktif")
    private String p_is_aktif;

    public String getP_kode() {
        return p_kode;
    }

    public void setP_kode(String p_kode) {
        this.p_kode = p_kode;
    }

    public String getP_tgl_libur() {
        return p_tgl_libur;
    }

    public void setP_tgl_libur(String p_tgl_libur) {
        this.p_tgl_libur = p_tgl_libur;
    }

    public String getP_keterangan() {
        return p_keterangan;
    }

    public void setP_keterangan(String p_keterangan) {
        this.p_keterangan = p_keterangan;
    }

    public String getP_is_aktif() {
        return p_is_aktif;
    }

    public void setP_is_aktif(String p_is_aktif) {
        this.p_is_aktif = p_is_aktif;
    }

    @Override
    public String toString() {
        return "HariLibur{" +
                "p_kode='" + p_kode + '\'' +
                ", p_tgl_libur='" + p_tgl_libur + '\'' +
                ", p_keterangan='" + p_keterangan + '\'' +
                ", p_is_aktif='" + p_is_aktif + '\'' +
                '}';
    }
}
