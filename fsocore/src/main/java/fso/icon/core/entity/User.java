package fso.icon.core.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by f.putra on 7/20/17.
 */
public class User extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -9045973522255445037L;
    private String username;
    private String password;
    private String nama;
    private String email;
    private String session;
    private String location;
    private String status;
    private String foto;
    private String phone;
    private String company;
    private boolean active;
    private String photo;
    //this for layanan mandiri
    private String tanggalBuat;
    private String lastLogin;
    private long idUser;
    private Long idLogUser;
    private Date timeLogin = new Date();
    private Date timeLogout = new Date();

    public User() {
    }

    public User(int id) {
        super(id);
    }

    public User(long id) {
        super(id);
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String nama) {
        this.nama = nama;
    }

    public User(Date createdTime, Date updatedTime, String username) {
        super(createdTime, updatedTime);
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getTanggalBuat() {
        return tanggalBuat;
    }

    public void setTanggalBuat(String tanggalBuat) {
        this.tanggalBuat = tanggalBuat;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    public Long getIdLogUser() {
        return idLogUser;
    }

    public void setIdLogUser(Long idLogUser) {
        this.idLogUser = idLogUser;
    }

    public Date getTimeLogin() {
        return timeLogin;
    }

    public void setTimeLogin(Date timeLogin) {
        this.timeLogin = timeLogin;
    }

    public Date getTimeLogout() {
        return timeLogout;
    }

    public void setTimeLogout(Date timeLogout) {
        this.timeLogout = timeLogout;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", nama='" + nama + '\'' +
                ", email='" + email + '\'' +
                ", session='" + session + '\'' +
                ", location='" + location + '\'' +
                ", status='" + status + '\'' +
                ", foto='" + foto + '\'' +
                ", phone='" + phone + '\'' +
                ", company='" + company + '\'' +
                ", active=" + active +
                ", photo='" + photo + '\'' +
                ", tanggalBuat='" + tanggalBuat + '\'' +
                ", lastLogin='" + lastLogin + '\'' +
                ", idUser=" + idUser +
                ", idLogUser=" + idLogUser +
                ", timeLogin=" + timeLogin +
                ", timeLogout=" + timeLogout +
                '}';
    }
}
