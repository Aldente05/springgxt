package fso.icon.core.entity;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;

/**
 * Created by f.putra on 7/27/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Petugas implements Serializable {

    private int is_insert;
    @JsonProperty("ID_USER")
    private String ID_USER;
    @JsonProperty("KODE_VENDOR")
    private String KODE_VENDOR;
    @JsonProperty("PASSWD")
    private String PASSWD;
    @JsonProperty("NAMA_USER")
    private String NAMA_USER;
    @JsonProperty("ALAMAT_USER")
    private String ALAMAT_USER;
    @JsonProperty("LEVELUSER")
    private String LEVELUSER;
    @JsonProperty("NO_HP")
    private String NO_HP;
    @JsonProperty("NO_IMEI")
    private String NO_IMEI;
    @JsonProperty("NO_TELP1")
    private String NO_TELP1;
    @JsonProperty("NO_TELP2")
    private String NO_TELP2;
    @JsonProperty("EMAIL1")
    private String EMAIL1;
    @JsonProperty("EMAIL2")
    private String EMAIL2;
    @JsonProperty("UNITUP")
    private String UNITUP;
    @JsonProperty("DISABLE_USER")
    private String DISABLE_USER;

    public Petugas(int is_insert, String ID_USER, String KODE_VENDOR, String PASSWD, String NAMA_USER, String ALAMAT_USER, String LEVELUSER, String NO_HP, String NO_IMEI, String NO_TELP1, String NO_TELP2, String EMAIL1, String EMAIL2, String UNITUP, String DISABLE_USER) {
        this.is_insert = is_insert;
        this.ID_USER = ID_USER;
        this.KODE_VENDOR = KODE_VENDOR;
        this.PASSWD = PASSWD;
        this.NAMA_USER = NAMA_USER;
        this.ALAMAT_USER = ALAMAT_USER;
        this.LEVELUSER = LEVELUSER;
        this.NO_HP = NO_HP;
        this.NO_IMEI = NO_IMEI;
        this.NO_TELP1 = NO_TELP1;
        this.NO_TELP2 = NO_TELP2;
        this.EMAIL1 = EMAIL1;
        this.EMAIL2 = EMAIL2;
        this.UNITUP = UNITUP;
        this.DISABLE_USER = DISABLE_USER;
    }

    public int getIs_insert() {
        return is_insert;
    }

    public void setIs_insert(int is_insert) {
        this.is_insert = is_insert;
    }

    public String getID_USER() {
        return ID_USER;
    }

    public void setID_USER(String ID_USER) {
        this.ID_USER = ID_USER;
    }

    public String getKODE_VENDOR() {
        return KODE_VENDOR;
    }

    public void setKODE_VENDOR(String KODE_VENDOR) {
        this.KODE_VENDOR = KODE_VENDOR;
    }

    public String getPASSWD() {
        return PASSWD;
    }

    public void setPASSWD(String PASSWD) {
        this.PASSWD = PASSWD;
    }

    public String getNAMA_USER() {
        return NAMA_USER;
    }

    public void setNAMA_USER(String NAMA_USER) {
        this.NAMA_USER = NAMA_USER;
    }

    public String getALAMAT_USER() {
        return ALAMAT_USER;
    }

    public void setALAMAT_USER(String ALAMAT_USER) {
        this.ALAMAT_USER = ALAMAT_USER;
    }

    public String getLEVELUSER() {
        return LEVELUSER;
    }

    public void setLEVELUSER(String LEVELUSER) {
        this.LEVELUSER = LEVELUSER;
    }

    public String getNO_HP() {
        return NO_HP;
    }

    public void setNO_HP(String NO_HP) {
        this.NO_HP = NO_HP;
    }

    public String getNO_IMEI() {
        return NO_IMEI;
    }

    public void setNO_IMEI(String NO_IMEI) {
        this.NO_IMEI = NO_IMEI;
    }

    public String getNO_TELP1() {
        return NO_TELP1;
    }

    public void setNO_TELP1(String NO_TELP1) {
        this.NO_TELP1 = NO_TELP1;
    }

    public String getNO_TELP2() {
        return NO_TELP2;
    }

    public void setNO_TELP2(String NO_TELP2) {
        this.NO_TELP2 = NO_TELP2;
    }

    public String getEMAIL1() {
        return EMAIL1;
    }

    public void setEMAIL1(String EMAIL1) {
        this.EMAIL1 = EMAIL1;
    }

    public String getEMAIL2() {
        return EMAIL2;
    }

    public void setEMAIL2(String EMAIL2) {
        this.EMAIL2 = EMAIL2;
    }

    public String getUNITUP() {
        return UNITUP;
    }

    public void setUNITUP(String UNITUP) {
        this.UNITUP = UNITUP;
    }

    public String getDISABLE_USER() {
        return DISABLE_USER;
    }

    public void setDISABLE_USER(String DISABLE_USER) {
        this.DISABLE_USER = DISABLE_USER;
    }

    @Override
    public String toString() {
        return "Petugas{" +
                "is_insert=" + is_insert +
                ", ID_USER='" + ID_USER + '\'' +
                ", KODE_VENDOR='" + KODE_VENDOR + '\'' +
                ", PASSWD='" + PASSWD + '\'' +
                ", NAMA_USER='" + NAMA_USER + '\'' +
                ", ALAMAT_USER='" + ALAMAT_USER + '\'' +
                ", LEVELUSER='" + LEVELUSER + '\'' +
                ", NO_HP='" + NO_HP + '\'' +
                ", NO_IMEI='" + NO_IMEI + '\'' +
                ", NO_TELP1='" + NO_TELP1 + '\'' +
                ", NO_TELP2='" + NO_TELP2 + '\'' +
                ", EMAIL1='" + EMAIL1 + '\'' +
                ", EMAIL2='" + EMAIL2 + '\'' +
                ", UNITUP='" + UNITUP + '\'' +
                ", DISABLE_USER='" + DISABLE_USER + '\'' +
                '}';
    }
}
