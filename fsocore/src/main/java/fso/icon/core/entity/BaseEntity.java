package fso.icon.core.entity;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by kris on 14/05/14.
 * base entity class providing standard property to use in persistent media
 */
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = -8528633195887266795L;
    private long id;
    private User createdBy;
    private Date createdTime = new Date();
    private User updatedBy;
    private Date updatedTime = new Date();
    //for custom searching from end user
    private String token;
    private String searchValue;
    private String searcKey;
    private Date searchStartDate;
    private Date searchEndDate;

    public BaseEntity() {
    }

    public BaseEntity(long id) {
        this.id = id;
    }

    public BaseEntity(Date createdTime, Date updatedTime) {
        this.createdTime = createdTime;
        this.updatedTime = updatedTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public String getSearcKey() {
        return searcKey;
    }

    public void setSearcKey(String searcKey) {
        this.searcKey = searcKey;
    }

    public Date getSearchStartDate() {
        return searchStartDate;
    }

    public void setSearchStartDate(Date searchStartDate) {
        this.searchStartDate = searchStartDate;
    }

    public Date getSearchEndDate() {
        return searchEndDate;
    }

    public void setSearchEndDate(Date searchEndDate) {
        this.searchEndDate = searchEndDate;
    }

    @Override
    public String toString() {
        return "BaseEntity{" +
                "id=" + id +
                ", createdBy=" + createdBy +
                ", createdTime=" + createdTime +
                ", updatedBy=" + updatedBy +
                ", updatedTime=" + updatedTime +
                ", token='" + token + '\'' +
                ", searchValue='" + searchValue + '\'' +
                ", searcKey='" + searcKey + '\'' +
                ", searchStartDate=" + searchStartDate +
                ", searchEndDate=" + searchEndDate +
                '}';
    }

}
