package fso.icon.core.common;

import java.util.Date;

/**
 * @author krissadewo
 *         Constant will used on the backend service for message or constant value service
 */
public class Constant {

    public static final String SESSION_USER = "user";
    public static final String FLAG_DELETE = "[DELETED] ".concat(new Date().getTime() + "");
    public static final Long NULL_ENTITY = null;
    public static final Long DEFAULT_ENTITY = 0l;
}
