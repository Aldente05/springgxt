package fso.icon.core.common;

import java.io.Serializable;

/**
 * Wrapper class for month
 */
public class Month implements Serializable {

    private static final long serialVersionUID = -2475194387671975679L;
    private String month;
    private String monthName;
    private Months months;

    public enum Months{
        Januari("01"),
        Februari("02"),
        Maret("03"),
        April("04"),
        Mei("05"),
        Juni("06"),
        Juli("07"),
        Agustus("08"),
        September("09"),
        Oktober("10"),
        November("11"),
        Desember("12");
        private String monthValue;

        private Months(String monthValue) {
            this.monthValue = monthValue;
        }

        public String getMonthValue() {
            return monthValue;
        }

    }

    public Month() {
    }

    public Month(String month, String monthName) {
        this.month = month;
        this.monthName = monthName;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public Months getMonths() {
        return months;
    }

    public void setMonths(Months months) {
        this.months = months;
    }

    @Override
    public String toString() {
        return "Month{" +
                "month=" + month +
                ", monthName='" + monthName + '\'' +
                '}';
    }
}