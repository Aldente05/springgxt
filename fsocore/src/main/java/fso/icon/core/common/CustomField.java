/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.core.common;

import java.io.Serializable;

/**
 * @author krissadewo
 *         <p/>
 *         This class handle for combo box that need to created label and label to the
 *         view
 */
public class CustomField implements Serializable {

    private String value;
    private String label;

    public CustomField() {
    }

    public CustomField(String value) {
        this.value = value;
    }

    public CustomField(String value, String label) {
        this.value = value;
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "CustomField{" + "value=" + value + ", label=" + label + '}';
    }
}
