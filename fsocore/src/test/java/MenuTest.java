
import fso.icon.core.entity.Menu;
import fso.icon.core.entity.User;
import fso.icon.core.service.NavigationMenuService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.*;


/**
 * Created by f.putra on 7/20/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:core-context.xml"})
@WebAppConfiguration
public class MenuTest {
    @Autowired
    private NavigationMenuService navigationMenuService;
    private User user;
    private Menu menuParent = new Menu();
    private List<Menu> menuChildern = new ArrayList<>();

    @Before
    public void init() {
        user = new User();
        user.setEmail("54.ADMIN");
    }

    @Test
    public void MenuList() {
        List<Menu> menus = navigationMenuService.menuByUser(user);
        int i = 1;
        for (Menu row : menus){
            if(0 == row.getIdParent()){
                menuParent.setIdParent(row.getIdParent());
                menuParent.setTitle(row.getTitle());
            } else {
                Menu child = new Menu();
                child.setIdParent(row.getIdParent());
                child.setTitle(row.getTitle());
                menuChildern.add(child);
                menuParent.setChildern(menuChildern);
            }

            System.out.println(menuParent.toString().replace("=", ":"));
        }
    }
}
