package fso.icon.app.client.service;


import com.google.gwt.user.client.rpc.AsyncCallback;
import fso.icon.core.entity.Menu;
import fso.icon.core.entity.User;

import java.util.ArrayList;

public interface GwtMenuServiceAsync {

    void createTreeMenu(AsyncCallback<ArrayList<Menu>> callback);

    void createTreeMenu(User role, AsyncCallback<ArrayList<Menu>> callback);
}
