/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.menu.settings;

import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.ComboBox;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.PasswordField;
import com.sencha.gxt.widget.core.client.form.TextField;
import fso.icon.app.client.view.custom.ComboBoxRole;
import fso.icon.app.client.view.custom.WindowFormView;
import fso.icon.app.client.view.handler.FormHandler;
import fso.icon.app.client.view.View;
import fso.icon.core.entity.Role;
import fso.icon.core.entity.User;

/**
 * @author krissadewo
 */
public class UserFormView extends View implements FormHandler {

    private WindowFormView windowFormView;
    private UserView parentView;
    private TextField textFieldNamaAsli;
    private TextField textFieldUsername;
    private PasswordField textFieldPassword;
    private ComboBox<Role> comboBoxRole;
    private User user;

    @Override
    public void showForm(View view) {
        parentView = (UserView) view;
        windowFormView = new WindowFormView();

        textFieldNamaAsli = new TextField();
        textFieldNamaAsli.setAllowBlank(false);

        textFieldUsername = new TextField();
        textFieldUsername.setAllowBlank(false);

        textFieldPassword = new PasswordField();
        textFieldPassword.setAllowBlank(false);

        comboBoxRole = (ComboBox) new ComboBoxRole().asWidget();

        if (parentView.getGrid().getSelectionModel().getSelectedItem() != null) {
            windowFormView.setHeading("Ubah Data");
            user = parentView.getGrid().getSelectionModel().getSelectedItem();
            textFieldNamaAsli.setValue(user.getNama());
            textFieldUsername.setValue(user.getUsername());
            textFieldPassword.setValue(user.getPassword());
//            comboBoxRole.setValue(user.getRole());
        } else {
            user = new User();
        }

        windowFormView.addWidget(new FieldLabel(textFieldNamaAsli, "Nama Asli"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.addWidget(new FieldLabel(textFieldUsername, "Username"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.addWidget(new FieldLabel(textFieldPassword, "Password"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.addWidget(new FieldLabel(comboBoxRole, "Role"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.getButtonSave().addSelectHandler(buttonSaveSelectHandler());
        windowFormView.setCursorPosition(textFieldNamaAsli);
        windowFormView.show();
    }

    @Override
    public SelectEvent.SelectHandler buttonSaveSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                if (!windowFormView.getFormPanel().isValid()) {
                    return;
                }

                user.setNama(textFieldNamaAsli.getValue());
                user.setUsername(textFieldUsername.getValue());
                user.setPassword(textFieldPassword.getValue());
//                user.setRole(comboBoxRole.getCurrentValue());
//                getService().getUserServiceAsync().save(user, new AsyncCallback<Result>() {
//
//                    @Override
//                    public void onFailure(Throwable caught) {
//                        AppClient.showMessageOnFailureException(caught);
//                    }
//
//                    @Override
//                    public void onSuccess(Result result) {
////                        if (result == Result.SAVE_SUCCESS) {
////                            windowFormView.hide();
////                            windowFormView.getFormPanel().reset();
////                            parentView.initGridData();
////                        } else {
////                            windowFormView.getButtonSave().setEnabled(true);
////                        }
//
//                        AppClient.showInfoMessage(result.getMessage());
//                    }
//                });
            }
        };
    }
}
