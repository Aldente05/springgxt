/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.properties;

import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor;

import java.math.BigDecimal;

/**
 * @author krissadewo
 */
public class DecimalPropertyEditor extends NumberPropertyEditor<BigDecimal> {

    public DecimalPropertyEditor(BigDecimal incrAmount) {
        super(incrAmount);
    }

    @Override
    protected BigDecimal doDecr(BigDecimal value) {
        return value.subtract(getIncrement());
    }

    @Override
    protected BigDecimal doIncr(BigDecimal value) {
        return value.add(getIncrement());
    }

    @Override
    protected BigDecimal parseString(String string) {
        return new BigDecimal(string);
    }

    @Override
    protected BigDecimal returnTypedValue(Number number) {
        if (number instanceof BigDecimal) {
            return (BigDecimal) number;
        } else {
            return new BigDecimal(number.doubleValue());
        }
    }

    @Override
    public String render(BigDecimal bigDecimal) {
        return null;
    }
}
