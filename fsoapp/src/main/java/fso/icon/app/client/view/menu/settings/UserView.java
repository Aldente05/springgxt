/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.menu.settings;

import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.Dialog;
import com.sencha.gxt.widget.core.client.box.ConfirmMessageBox;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.HideEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.filters.GridFilters;
import com.sencha.gxt.widget.core.client.grid.filters.StringFilter;
import fso.icon.app.client.view.custom.GridView;
import fso.icon.app.client.AppClient;
import fso.icon.app.client.view.View;
import fso.icon.app.client.view.custom.ToolbarGridView;
import fso.icon.app.client.view.handler.GridHandler;
import fso.icon.core.common.Result;
import fso.icon.core.entity.Menu;
import fso.icon.core.entity.User;

import java.util.ArrayList;

/**
 * @author krissadewo
 */
public class UserView extends View implements GridHandler {

    private Grid<User> grid;
    private ListStore<User> listStore;

    public UserView(Menu menu) {
        super(menu);
    }

    @Override
    public Widget asWidget() {
        listStore = new ListStore<User>(getProperties().getUserProperties().key());

        ColumnConfig<User, String> colNamaAsli = new ColumnConfig<User, String>(getProperties().getUserProperties().valueRealName(), 200, "NAMA");
        ColumnConfig<User, String> colUsername = new ColumnConfig<User, String>(getProperties().getUserProperties().valueUsername(), 200, "USERNAME");
        ColumnConfig<User, String> colNamaLevel = new ColumnConfig<User, String>(getProperties().getUserProperties().valueNamaRole(), 100, "ROLE");

        ArrayList<ColumnConfig<User, ?>> list = new ArrayList<ColumnConfig<User, ?>>();
        list.add(colNamaAsli);
        list.add(colUsername);
        list.add(colNamaLevel);
        ColumnModel<User> columnModel = new ColumnModel<User>(list);

        grid = new Grid<User>(listStore, columnModel);
        grid.getView().setStripeRows(true);
        grid.getView().setColumnLines(true);
        grid.getView().setAutoFill(true);

        StringFilter<User> namaFilter = new StringFilter<User>(getProperties().getUserProperties().valueRealName());
        StringFilter<User> usernameFilter = new StringFilter<User>(getProperties().getUserProperties().valueUsername());
        StringFilter<User> levelFilter = new StringFilter<User>(getProperties().getUserProperties().valueNamaRole());

        GridFilters<User> gridFilters = new GridFilters<User>();
        gridFilters.initPlugin(grid);
        gridFilters.setLocal(true);
        gridFilters.addFilter(namaFilter);
        gridFilters.addFilter(usernameFilter);
        gridFilters.addFilter(levelFilter);

        GridView gridView = new GridView();
        gridView.addWidget(createToolbar(), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        gridView.addWidget(grid, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        return gridView;
    }

    private Widget createToolbar() {
        return new ToolbarGridView(super.getMenu()) {
            @Override
            public SelectEvent.SelectHandler buttonToolbarAddSelectHandler() {
                return buttonAddSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarEditSelectHandler() {
                return buttonEditSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarDeleteSelectHandler() {
                return buttonDeleteSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarSearchSelectHandler() {
                return buttonSearchSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarPrintSelectHandler() {
                return buttonPrintSelectHandler();
            }
        }.asWidget();
    }

    @Override
    public void initGridData() {
//        getService().getUserServiceAsync().findAll(new AsyncCallback<ArrayList<User>>() {
//            @Override
//            public void onFailure(Throwable caught) {
//                AppClient.showMessageOnFailureException(caught);
//            }
//
//            @Override
//            public void onSuccess(ArrayList<User> result) {
//                listStore.replaceAll(result);
//            }
//        });
    }

    @Override
    public SelectEvent.SelectHandler buttonAddSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                grid.getSelectionModel().deselectAll();
                new UserFormView().showForm(UserView.this);
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonEditSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                if (grid.getSelectionModel().getSelectedItem() != null) {
                    new UserFormView().showForm(UserView.this);
                } else {
                    AppClient.showInfoMessage(Result.DATA_NOT_SELECTED);
                }
                grid.getSelectionModel().deselectAll();
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonDeleteSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                final User user = grid.getSelectionModel().getSelectedItem();
                if (user == null) {
                    AppClient.showInfoMessage(Result.DATA_NOT_SELECTED);
                    return;
                }

                ConfirmMessageBox confirmMessageBox = AppClient.showMessageConfirmForDelete();
                confirmMessageBox.addHideHandler(new HideEvent.HideHandler() {
                    @Override
                    public void onHide(HideEvent event) {
                        Dialog dialog = (Dialog) event.getSource();
//                        String message = Format.substitute("{0}", dialog.getHideButton().getText());
//                        if (message.equals(AppClient.MESSAGE_YES)) {
//                            getService().getUserServiceAsync().delete(user, new AsyncCallback<Result>() {
//                                @Override
//                                public void onFailure(Throwable caught) {
//                                }
//
//                                @Override
//                                public void onSuccess(Result result) {
//                                    AppClient.showInfoMessage(result.getMessage());
//                                    if (result == Result.DELETE_SUCCESS) {
//                                        initGridData();
//                                    }
//                                }
//                            });
//                        } else {
//                            grid.getSelectionModel().deselectAll();
//                        }
                    }
                });
                confirmMessageBox.show();
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonSearchSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {

//                AppClient.showInfoMessage(Result.SEARCH_NOT_HERE);
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonPrintSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        };
    }

    public Grid<User> getGrid() {
        return grid;
    }


}
