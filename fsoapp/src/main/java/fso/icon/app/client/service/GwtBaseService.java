/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.service;

import com.google.gwt.core.shared.GWT;

/**
 * @author kris
 */
public class GwtBaseService {

    private final GwtSessionHandlerServiceAsync sessionHandlerServiceAsync = GWT.create(GwtSessionHandlerService.class);
    private final GwtMenuServiceAsync menuServiceAsync = GWT.create(GwtMenuService.class);
    private final GwtHomeServiceAsync homeServiceAsync = GWT.create(GwtHomeService.class);

    private GwtBaseService() {
    }

    public static GwtBaseService getInstance() {
        return BaseServiceHolder.INSTANCE;
    }

    private static class BaseServiceHolder {
        private static final GwtBaseService INSTANCE = new GwtBaseService();
    }

    public GwtSessionHandlerServiceAsync getSessionHandlerServiceAsync() {
        return sessionHandlerServiceAsync;
    }

    public GwtMenuServiceAsync getMenuServiceAsync() {
        return menuServiceAsync;
    }

    public GwtHomeServiceAsync getHomeServiceAsync() {
        return homeServiceAsync;
    }
}
