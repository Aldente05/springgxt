package fso.icon.app.client.view.custom;

import com.google.gwt.user.client.ui.Frame;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.form.FormPanel;

/**
 * @author krissadewo
 */
public class WindowReportView extends Window {

    private FormPanel formPanel;
    private Frame frame;

    public WindowReportView(String url) {
        frame = new Frame();
        frame.setUrl(url);

        formPanel = new FormPanel();
        formPanel.add(frame, new MarginData(5, 2, 0, 2));

        super.setWidth("800");
        super.setHeight("500");
        super.setClosable(true);
        super.setModal(true);
        super.setShadow(true);
        super.add(formPanel);
    }

}

