/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.custom;

import com.google.gwt.user.client.ui.Widget;
import fso.icon.core.common.CustomField;
import fso.icon.app.client.view.View;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.form.ComboBox;
import fso.icon.core.common.Month;

import java.util.ArrayList;
import java.util.List;

/**
 * @author krissadewo
 *         <p/>
 *         The generic way to fill for any requirement for combo box
 */
public class ComboBoxCustom extends View {

    private ComboBoxType comboBoxType;
    private CustomField customField;

    public ComboBoxCustom(ComboBoxType comboBoxType) {
        this.comboBoxType = comboBoxType;
    }

    public ComboBoxCustom(ComboBoxType comboBoxType, CustomField customField) {
        this.comboBoxType = comboBoxType;
        this.customField = customField;
    }

    @Override
    public Widget asWidget() {
        ListStore<CustomField> listStore = new ListStore<CustomField>(getProperties().getCustomFieldProperties().key());
        ComboBox<CustomField> comboBox = new ComboBox<CustomField>(listStore, getProperties().getCustomFieldProperties().labelName());

        List<CustomField> customFields = new ArrayList<CustomField>();
        if (comboBoxType == ComboBoxType.MONTH) {
            customFields.add(new CustomField(Month.Months.Januari.getMonthValue(), Month.Months.Januari.name()));
            customFields.add(new CustomField(Month.Months.Februari.getMonthValue(), Month.Months.Februari.name()));
            customFields.add(new CustomField(Month.Months.Maret.getMonthValue(), Month.Months.Maret.name()));
            customFields.add(new CustomField(Month.Months.April.getMonthValue(), Month.Months.April.name()));
            customFields.add(new CustomField(Month.Months.Mei.getMonthValue(), Month.Months.Mei.name()));
            customFields.add(new CustomField(Month.Months.Juni.getMonthValue(), Month.Months.Juni.name()));
            customFields.add(new CustomField(Month.Months.Juli.getMonthValue(), Month.Months.Juli.name()));
            customFields.add(new CustomField(Month.Months.Agustus.getMonthValue(), Month.Months.Agustus.name()));
            customFields.add(new CustomField(Month.Months.September.getMonthValue(), Month.Months.September.name()));
            customFields.add(new CustomField(Month.Months.Oktober.getMonthValue(), Month.Months.Oktober.name()));
            customFields.add(new CustomField(Month.Months.November.getMonthValue(), Month.Months.November.name()));
            customFields.add(new CustomField(Month.Months.Desember.getMonthValue(), Month.Months.Desember.name()));
        }

        if (customField != null) {
            comboBox.setValue(customField);
        } else {
            comboBox.setValue(customFields.get(0));
        }

        comboBox.setEditable(false);
        comboBox.setOriginalValue(comboBox.getValue());
        comboBox.setAutoValidate(true);
        comboBox.setEmptyText("pilih data");
        comboBox.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
        comboBox.setForceSelection(true);
        return comboBox;
    }

}
