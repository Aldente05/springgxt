package fso.icon.app.client.view;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import fso.icon.app.client.service.GwtBaseService;
import fso.icon.app.client.view.properties.BaseProperties;
import fso.icon.core.entity.Menu;

/**
 * @author krissadewo
 *         <p />Base class for user view, providing base method to child class
 */
public abstract class View implements IsWidget {

    private Menu menu;
    //######################## MASTER ###############################
    public static final String VIEW_MASTER_BUKU = "VIEW_MASTER_BUKU";
    public static final String VIEW_MASTER_MAHASISWA = "VIEW_MASTER_MAHASISWA";
    //######################## SETTING ###############################
    public static final String VIEW_SETTING_HAK_AKSES = "VIEW_SETTING_HAK_AKSES";
    public static final String VIEW_SETTING_USER_MANAGEMENT = "VIEW_SETTING_USER_MANAGEMENT";


    public View() {
        init();
    }

    public View(Menu menu) {
        this.menu = menu;
        init();
    }

    /**
     * Call this method to load specific view
     *
     * @param view view
     * @return specific view
     */
    public static Widget loadView(View view) {
        return view.asWidget();
    }

    @Override
    public Widget asWidget() {
        return asWidget();
    }

    private void init() {
        initGridData();
    }

    /**
     * All about RPC service will be use on client application
     *
     * @return service
     */
    protected GwtBaseService getService() {
        return GwtBaseService.getInstance();
    }

    protected BaseProperties getProperties() {
        return BaseProperties.getInstance();
    }

    /**
     * Override this method with specific name of view. View name will be used
     * to selection for what view will be used when tree menu has changed
     *
     * @return
     */
    protected Menu getMenu() {
        return menu;
    }

    /**
     * This method can override to load grid data as default on the grid/(the sub class of this), you
     * don't need load data into the grid manually if you override this method
     * <p/>
     */
    protected void initGridData() {
    }

}
