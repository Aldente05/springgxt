/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.custom;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import fso.icon.core.entity.Role;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.form.ComboBox;

/**
 * @author krissadewo
 */
public abstract class AbstractComboBoxRole implements IsWidget {

    private ListStore<Role> listStore;
    private ComboBox<Role> comboBox;

    @Override
    public Widget asWidget() {
//        listStore = new ListStore<Role>(BaseProperties.getInstance().getRoleProperties().key());
        initData();

//        comboBox = new ComboBox<Role>(listStore, BaseProperties.getInstance().getRoleProperties().labelNama());
//        comboBox.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
//        comboBox.setWidth(120);
//        comboBox.setEditable(false);
//        comboBox.setForceSelection(true);
        return comboBox;
    }

    private void initData() {
//        GwtBaseService.getInstance().getRoleServiceAsync().findAll(new AsyncCallback<ArrayList<Role>>() {
//            @Override
//            public void onFailure(Throwable caught) {
//                AppClient.showMessageOnFailureException(caught);
//            }
//
//            @Override
//            public void onSuccess(ArrayList<Role> result) {
//                listStore.replaceAll(result);
//                if (comboBox.getValue() == null) {
//                    comboBox.setValue(listStore.get(0));
//                }
//                loadRelatedMenu();
//            }
//        });
    }

    public abstract void loadRelatedMenu();
}
