/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.custom;

import fso.icon.app.client.icon.Icon;
import fso.icon.core.entity.Menu;
import com.sencha.gxt.cell.core.client.ButtonCell;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.toolbar.SeparatorToolItem;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

/**
 * @author krissadewo
 */
public abstract class ToolbarGridView extends ToolBar {

    public ToolbarGridView(Menu menu) {
        init(menu, null);
    }

    /**
     * @param menu        menu ref
     * @param toolbarType specific toolbar type, can be null
     */
    public ToolbarGridView(Menu menu, ToolbarType toolbarType) {
        init(menu, toolbarType);
    }

    private void init(Menu menu, ToolbarType toolbarType) {
        super.setPack(BoxLayoutContainer.BoxLayoutPack.END);
        super.add(new SeparatorToolItem());
        super.setSpacing(2);

        //We can custom anything at here
//        if (menu.getUserMenu().isCanSave()) {
//            TextButton textButtonAdd = new TextButton("Tambah", buttonToolbarAddSelectHandler());
//            textButtonAdd.setIcon(Icon.INSTANCE.add());
//            textButtonAdd.setIconAlign(ButtonCell.IconAlign.RIGHT);
//            super.add(textButtonAdd);
//            super.add(new SeparatorToolItem());
//        }
//
//        if (menu.getUserMenu().isCanEdit()) {
//            TextButton textButtonEdit = new TextButton("Ubah", buttonToolbarEditSelectHandler());
//            textButtonEdit.setIcon(Icon.INSTANCE.edit());
//            textButtonEdit.setIconAlign(ButtonCell.IconAlign.RIGHT);
//            super.add(textButtonEdit);
//            super.add(new SeparatorToolItem());
//        }
//
//        if (menu.getUserMenu().isCanDelete()) {
//            TextButton textButtonDelete = new TextButton("Hapus", buttonToolbarDeleteSelectHandler());
//            textButtonDelete.setIcon(Icon.INSTANCE.delete());
//            textButtonDelete.setIconAlign(ButtonCell.IconAlign.RIGHT);
//            super.add(textButtonDelete);
//            super.add(new SeparatorToolItem());
//        }

        if (toolbarType == ToolbarType.CUSTOM_TOOLBAR) {
            TextButton textButtonSync = new TextButton("Custom Toolbar", buttonToolbarSyncSelectHandler());
            textButtonSync.setIcon(Icon.INSTANCE.sync());
            textButtonSync.setIconAlign(ButtonCell.IconAlign.RIGHT);
            super.add(textButtonSync);
            super.add(new SeparatorToolItem());
        }

        TextButton textButtonSearch = new TextButton("Cari", buttonToolbarSearchSelectHandler());
        textButtonSearch.setIcon(Icon.INSTANCE.search());
        textButtonSearch.setIconAlign(ButtonCell.IconAlign.RIGHT);
        super.add(textButtonSearch);
        super.add(new SeparatorToolItem());

//        if (menu.getUserMenu().isCanPrint()) {
//            TextButton textButtonPrint = new TextButton("Unduh", buttonToolbarPrintSelectHandler());
//            textButtonPrint.setIcon(Icon.INSTANCE.exportExcel());
//            textButtonPrint.setIconAlign(ButtonCell.IconAlign.RIGHT);
//            super.add(textButtonPrint);
//            super.add(new SeparatorToolItem());
//        }
    }

    public abstract SelectEvent.SelectHandler buttonToolbarAddSelectHandler();

    public abstract SelectEvent.SelectHandler buttonToolbarEditSelectHandler();

    public abstract SelectEvent.SelectHandler buttonToolbarDeleteSelectHandler();

    public abstract SelectEvent.SelectHandler buttonToolbarSearchSelectHandler();

    public abstract SelectEvent.SelectHandler buttonToolbarPrintSelectHandler();

    public SelectEvent.SelectHandler buttonToolbarSyncSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
            }
        };
    }
}
