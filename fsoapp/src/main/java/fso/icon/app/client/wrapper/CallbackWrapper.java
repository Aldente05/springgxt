/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.wrapper;

import com.google.gwt.user.client.rpc.AsyncCallback;
import fso.icon.app.client.AppClient;

/**
 *
 * @author krissadewo <dailycode.org>
 * @date Jul 1, 2013
 * <p>Base class for wrap AsyncCallback class to handle boiler plate code, Used
 * this class on asyncCallback will called any super implementation method.
 */
public abstract class CallbackWrapper<T> {

    private native void loadMessage()/*-{
       
     $wnd.$(document).ready(function() { 
     
     $wnd.$.blockUI({ css: { 
     border: 'none', 
     padding: '15px', 
     backgroundColor: '#000', 
     '-webkit-border-radius': '10px', 
     '-moz-border-radius': '10px',
     opacity: .5, 
     color: '#fff'   
     } 
     }); 
     //setTimeout($wnd.$.unblockUI, 10000);     
     });
     }-*/;

    private native void hideMessage()/*-{
     $wnd.$(document).ready(function() {      
     setTimeout($wnd.$.unblockUI, 500);
     });
     }-*/;
    private AsyncCallback<T> asyncCallback = new AsyncCallback<T>() {
        @Override
        public void onFailure(Throwable caught) {
            AppClient.showMessageOnFailureException(caught);
            CallbackWrapper.this.onFailure(caught);
            hideMessage();
        }

        @Override
        public void onSuccess(T result) {
            CallbackWrapper.this.onSuccess(result);
            hideMessage();
        }
    };

    protected final AsyncCallback<T> getAsyncCallback() {
        return asyncCallback;
    }

    protected abstract void onSuccess(T result);

    protected abstract void onFailure(Throwable throwable);

    protected abstract void onCall(AsyncCallback<T> callback);

    public final void call() {
        loadMessage();
        onCall(getAsyncCallback());
    }
}
