package fso.icon.app.client;

import com.google.gwt.i18n.client.NumberFormat;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.box.ConfirmMessageBox;
import com.sencha.gxt.widget.core.client.box.MessageBox;
import fso.icon.core.common.Result;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by f.putra on 7/31/17.
 */
public class AppClient {

    public static final String MESSAGE_YES = "Yes";
    public static final Integer PAGING_SIZE = 100;

    /**
     * return dd-mm-yyyy hh:mm:ss
     */
    public static final String DATE_TIME_PATTERN = "dd-MM-yyyy hh:mm:ss";
    /**
     * return dd-MMMM-yyyy
     */
    public static final String DATE_PATTERN_WITH_MONTH_NAME = "dd-MMMM-yyyy";
    /**
     * return dd-mm-yyyy
     */
    public static final String DATE_PATTERN = "dd-MM-yyyy";
    public static final NumberFormat CURRENCY_FORMAT = NumberFormat.getFormat("#,###,###,##0.00");
    public static final NumberFormat SIMPLE_FORMAT = NumberFormat.getFormat("#0.00");

    public static AppClient getInstance() {
        return AppClientHolder.INSTANCE;
    }

    public static void showMessageOnFailureException(Throwable throwable) {
        AlertMessageBox box = new AlertMessageBox("Error", "Mungkin terjadi masalah pada jaringan anda, periksa lagi apakah jaringan anda...");
        box.setIcon(MessageBox.ICONS.error());
        box.show();
    }

    public static ConfirmMessageBox showMessageConfirmForDelete() {
        return new ConfirmMessageBox("Confirm", "Hapus data ini ?");
    }

    public static ConfirmMessageBox showMessageConfirmForTutupBuku() {
        return new ConfirmMessageBox("Confirm", "Jalankan penutupan buku ?");
    }

    public static ConfirmMessageBox showMessageConfirmForVerifikasi() {
        return new ConfirmMessageBox("Confirm", "Verifikasi data ini ?");
    }

    public static ConfirmMessageBox showMessageConfirmForRekap() {
        return new ConfirmMessageBox("Confirm", "Rekap data mungkin membutuhkan waktu yang lama. Jalankan rekap data ? ");
    }

    public static MessageBox showInfoMessage(Result message) {
        AlertMessageBox box = new AlertMessageBox("Informasi", message.getMessage());
        box.setIcon(MessageBox.ICONS.info());
        box.show();
        return box;
    }

    public static MessageBox showInfoMessage(String message) {
        AlertMessageBox box = new AlertMessageBox("Informasi", message);
        box.setIcon(MessageBox.ICONS.info());
        box.show();
        return box;
    }

    /**
     * This is standard method to divide BigDecimal value
     *
     * @param x x
     * @param y y
     * @return devide value
     */
    public static BigDecimal divide(BigDecimal x, BigDecimal y) {
        return x.divide(y, 2, RoundingMode.HALF_EVEN);
    }

    private static class AppClientHolder {

        private static final AppClient INSTANCE = new AppClient();
    }
}
