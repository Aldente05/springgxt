/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.custom;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.Widget;
import fso.icon.app.client.icon.Icon;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.menu.Menu;
import com.sencha.gxt.widget.core.client.menu.MenuItem;

/**
 * @author krissadewo
 *         Provide standard grid panel to used on grid view
 */
public class GridView extends ContentPanel {

    private VerticalLayoutContainer verticalLayoutContainer = new VerticalLayoutContainer();
    private Menu menu = new Menu();

    public GridView() {
        activeContextMenu(null);

        super.setHeaderVisible(true);
        super.setShadow(true);
        super.getHeader().setIcon(Icon.INSTANCE.grid());
        super.add(verticalLayoutContainer);
        super.setContextMenu(menu);
    }

    /**
     * Add other widget and layout data to attach on this panel
     *
     * @param widget widget
     * @param data   data
     */
    public void addWidget(Widget widget, VerticalLayoutContainer.VerticalLayoutData data) {
        verticalLayoutContainer.add(widget, data);
    }

    public void activeContextMenu(ContextMenuType contextMenuType) {
        if (contextMenuType == null) {
            MenuItem menuItem = new MenuItem();
            menuItem.setText(ContextMenuType.MENU_INFO.getName());
            menuItem.setIcon(Icon.INSTANCE.info());
            menu.add(menuItem);
        } else {
            if (contextMenuType == ContextMenuType.MENU_REFRESH) {
                MenuItem menuItem = new MenuItem();
                menuItem.setText(ContextMenuType.MENU_REFRESH.getName());
                menuItem.setIcon(Icon.INSTANCE.refresh());
                menuItem.addSelectionHandler(addRefreshSelectionHandler());
                menu.add(menuItem);
            } else {
                MenuItem menuItem = new MenuItem();
                menuItem.setText(ContextMenuType.MENU_INFO.getName());
                menuItem.setIcon(Icon.INSTANCE.info());
                menu.add(menuItem);
            }
        }

    }

    public enum ContextMenuType {

        MENU_REFRESH("REFRESH"),
        MENU_INFO("MENU NOT AVAILABLE");

        private String name;

        ContextMenuType(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public SelectionHandler addRefreshSelectionHandler() {
        return new SelectionHandler() {
            @Override
            public void onSelection(SelectionEvent event) {
            }
        };
    }
}
