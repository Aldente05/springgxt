package fso.icon.app.client.view.custom;

import com.google.gwt.user.client.ui.Frame;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.toolbar.SeparatorToolItem;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;


/**
 * Created with IntelliJ IDEA.
 * User: krissadewo
 * Date: 11/24/13
 * Time: 4:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class ReportViewer extends Window {

    private Frame frame;
    private String url;

    public ReportViewer(String url) {
        this.url = url;
        super.setModal(true);
        super.setPixelSize(getScreenWidth() - 200, getScreenHeight() - 200);
        VerticalLayoutContainer verticalLayoutContainer = new VerticalLayoutContainer();

        frame = new Frame();
        frame.setUrl(url);

        TextButton textButtonExport = new TextButtonExport();
        textButtonExport.addSelectHandler(textButtonExportSelectHandler());

        ToolBar toolBar = new ToolBar();
        toolBar.setPack(BoxLayoutContainer.BoxLayoutPack.END);
        toolBar.add(new SeparatorToolItem());
        toolBar.setSpacing(2);
        toolBar.add(textButtonExport);

        System.out.println(frame.getUrl());
        verticalLayoutContainer.add(toolBar, new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        verticalLayoutContainer.add(frame, new VerticalLayoutContainer.VerticalLayoutData(1, 1));

        super.add(verticalLayoutContainer, new MarginData(5, 2, 0, 2));
    }

    public static native int getScreenWidth() /*-{
     return $wnd.screen.width;
     }-*/;

    public static native int getScreenHeight() /*-{
     return $wnd.screen.height;
     }-*/;

    private SelectEvent.SelectHandler textButtonExportSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                //temporary solusition, make it better
                frame.setUrl(url.replace("pdf", "xls"));
            }
        };
    }
}
