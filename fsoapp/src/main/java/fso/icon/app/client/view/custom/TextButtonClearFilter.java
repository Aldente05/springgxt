/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.custom;

import com.sencha.gxt.widget.core.client.button.TextButton;

/**
 * @author krissadewo <dailycode.org>
 * @date Jul 15, 2013
 */
public class TextButtonClearFilter extends TextButton {

    public TextButtonClearFilter() {
        super("Clear Filter");
    }
}
