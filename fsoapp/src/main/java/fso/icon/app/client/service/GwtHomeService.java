/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * @author krissadewo <dailycode.org>
 * @date Jul 18, 2013
 */
@RemoteServiceRelativePath("springGwtServices/gwtHomeService")
public interface GwtHomeService extends RemoteService {

    String getAppTitle();
}
