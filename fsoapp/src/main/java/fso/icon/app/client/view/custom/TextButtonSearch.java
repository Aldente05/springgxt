/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.custom;

import fso.icon.app.client.icon.Icon;
import com.sencha.gxt.cell.core.client.ButtonCell;
import com.sencha.gxt.widget.core.client.button.TextButton;

/**
 * @author krissadewo <dailycode.org>
 * @date Jul 15, 2013
 */
public class TextButtonSearch extends TextButton {

    public TextButtonSearch(String text) {
        super(text);
        super.setIcon(Icon.INSTANCE.search());
        super.setIconAlign(ButtonCell.IconAlign.RIGHT);
    }

    public TextButtonSearch() {
        super("Pencarian");
        super.setIcon(Icon.INSTANCE.search());
        super.setIconAlign(ButtonCell.IconAlign.RIGHT);
    }
}
