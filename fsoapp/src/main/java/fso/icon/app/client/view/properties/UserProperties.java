/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.properties;

import com.google.gwt.editor.client.Editor.Path;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import fso.icon.core.entity.User;

/**
 * @author krissadewo
 */
public interface UserProperties extends PropertyAccess<User> {

    @Path("id")
    ModelKeyProvider<User> key();

    @Path("realname")
    ValueProvider<User, String> valueRealName();

    @Path("username")
    LabelProvider<User> labelUsername();

    @Path("realname")
    LabelProvider<User> labelRealname();

    @Path("username")
    ValueProvider<User, String> valueUsername();

    @Path("password")
    ValueProvider<User, String> valuePassword();

    @Path("role.nama")
    ValueProvider<User, String> valueNamaRole();
}
