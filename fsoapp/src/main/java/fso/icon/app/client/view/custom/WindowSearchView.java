/*
 * To change verticalLayoutContainer template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.custom;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import fso.icon.core.common.CustomField;
import fso.icon.core.common.Month;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.HorizontalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.ComboBox;
import com.sencha.gxt.widget.core.client.form.DateField;
import com.sencha.gxt.widget.core.client.form.FormPanel;
import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor;
import com.sencha.gxt.widget.core.client.toolbar.SeparatorToolItem;

import java.util.Date;

/**
 * @author krissadewo
 */
public abstract class WindowSearchView extends Window {

    private FramedPanel framedPanel;
    private FormPanel formPanel;
    private VerticalLayoutContainer verticalLayoutContainer;
    private TextButton buttonSearch;
    private TextButton buttonReset;
    private DateField dateFieldTanggalAwal = new DateField();
    private DateField dateFieldTanggalAkhir = new DateField();
    private ComboBox<CustomField> comboBoxJenisPencarian;
    private ComboBox<Month> comboBoxMonth;
    private FieldYear fieldYear;

    public WindowSearchView(ToolbarSearchType toolbarSearchType) {
        super.setMinHeight(290);
        super.setHeight(290);
        super.setHeading("Pencarian");
        super.setWidth("500");
        super.setShadow(true);
        super.setModal(true);

        verticalLayoutContainer = new VerticalLayoutContainer();
        formPanel = new FormPanel();
        formPanel.add(verticalLayoutContainer, new MarginData(5, 2, 0, 2));

        framedPanel = new FramedPanel();
        framedPanel.setHeaderVisible(true);
        framedPanel.add(formPanel);

        buttonSearch = new TextButtonSearch("Cari");
        buttonSearch.addSelectHandler(buttonSearchHandler());
        framedPanel.addButton(buttonSearch);

        buttonReset = new TextButtonReset();
        buttonReset.addSelectHandler(buttonRefreshResetHandler());
        framedPanel.addButton(buttonReset);

        dateFieldTanggalAwal.setValue(new Date());
        dateFieldTanggalAwal.setEditable(false);
        dateFieldTanggalAwal.getDatePicker().addValueChangeHandler(new ValueChangeHandler<Date>() {
            @Override
            public void onValueChange(ValueChangeEvent<Date> event) {
                dateFieldTanggalAwal.setValue(event.getValue());
            }
        });

        dateFieldTanggalAkhir.setValue(new Date());
        dateFieldTanggalAkhir.setEditable(false);
        dateFieldTanggalAkhir.getDatePicker().addValueChangeHandler(new ValueChangeHandler<Date>() {
            @Override
            public void onValueChange(ValueChangeEvent<Date> event) {
                dateFieldTanggalAkhir.setValue(event.getValue());
            }
        });

        final HorizontalLayoutContainer horizontalPeriodeDate = new HorizontalLayoutContainer();
        horizontalPeriodeDate.setLayoutData(new HorizontalLayoutContainer.HorizontalLayoutData(1, -1, new Margins(-3, 0, 4, 0)));
        horizontalPeriodeDate.add(dateFieldTanggalAwal);
        horizontalPeriodeDate.add(new SeparatorToolItem());
        horizontalPeriodeDate.add(dateFieldTanggalAkhir);

        fieldYear = new FieldYear(new NumberPropertyEditor.IntegerPropertyEditor());
        comboBoxMonth = (ComboBox<Month>) new ComboBoxMonth().asWidget();
        comboBoxMonth.setWidth(120);

        final HorizontalLayoutContainer horizontalPeriodeMonth = new HorizontalLayoutContainer();
        horizontalPeriodeMonth.setLayoutData(new HorizontalLayoutContainer.HorizontalLayoutData(1, -1));
        horizontalPeriodeMonth.add(comboBoxMonth);
        horizontalPeriodeMonth.add(new SeparatorToolItem());
        horizontalPeriodeMonth.add(fieldYear);

        super.setResizable(false);
        super.add(framedPanel, new MarginData(5, 3, 1, 3));
    }


    private SelectEvent.SelectHandler buttonRefreshResetHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                reset();
            }
        };
    }

    public abstract SelectEvent.SelectHandler buttonSearchHandler();

    public void reset() {
        formPanel.reset();
        dateFieldTanggalAwal.setValue(new Date());
        dateFieldTanggalAkhir.setValue(new Date());

        if (comboBoxJenisPencarian != null)
            comboBoxJenisPencarian.setValue(comboBoxJenisPencarian.getStore().get(0));

    }

}
