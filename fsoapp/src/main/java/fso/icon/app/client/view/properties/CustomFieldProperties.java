/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.properties;

import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import fso.icon.core.common.CustomField;

/**
 * @author krissadewo
 */
public interface CustomFieldProperties extends PropertyAccess<CustomField> {

    @Editor.Path("value")
    ModelKeyProvider<CustomField> key();

    @Editor.Path("label")
    LabelProvider<CustomField> labelName();
}
