/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.custom;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.core.client.dom.ScrollSupport;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.HideEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.FormPanel;
import com.sencha.gxt.widget.core.client.form.TextField;

/**
 * @author krissadewo
 *         <p/>
 *         Provide standard form input with save and cancel functionality
 */
public class WindowFormView extends Window {

    private final VerticalLayoutContainer verticalLayoutContainer;
    private final TextButton buttonSave;
    private final TextButton buttonReset;
    private FramedPanel framedPanel;
    private FormPanel formPanel;
    private WindowFormView windowFormView;

    public WindowFormView() {
        windowFormView = this;
        verticalLayoutContainer = new VerticalLayoutContainer();
        formPanel = new FormPanel();
        formPanel.add(verticalLayoutContainer, new MarginData(5, 2, 0, 2));

        framedPanel = new FramedPanel();
        framedPanel.setHeading("Form Input Data");
        framedPanel.add(formPanel);

        buttonSave = new TextButtonSave();
        buttonSave.addSelectHandler(buttonSaveSelectHandler());

        buttonReset = new TextButtonReset();
        buttonReset.addSelectHandler(buttonResetSelectHandler());
        buttonReset.addHandler(buttonResetKeyDownHandler(), KeyDownEvent.getType());

        framedPanel.addButton(buttonSave);
        framedPanel.addButton(buttonReset);
        verticalLayoutContainer.setScrollMode(ScrollSupport.ScrollMode.AUTOY);
        verticalLayoutContainer.setAdjustForScroll(true);

        super.addHideHandler(new HideEvent.HideHandler() {
            @Override
            public void onHide(HideEvent event) {
                formPanel.reset();
            }
        });

        super.setResizable(false);
        super.setWidth("500");
        super.setClosable(true);
        super.setModal(true);
        super.setShadow(true);
        super.add(framedPanel);
    }

    /**
     * Add widget to the vertical layout container
     *
     * @param widget             widget
     * @param verticalLayoutData vertical layout
     */
    public void addWidget(Widget widget, VerticalLayoutContainer.VerticalLayoutData verticalLayoutData) {
        verticalLayoutContainer.add(widget, verticalLayoutData);
    }

    public TextButton getButtonSave() {
        return buttonSave;
    }

    public TextButton getButtonReset() {
        return buttonReset;
    }

    public FormPanel getFormPanel() {
        return formPanel;
    }

    public FramedPanel getFramedPanel() {
        return framedPanel;
    }

    /**
     * set default cursor position,
     *
     * @param widget widget to default position
     */
    public void setCursorPosition(final Widget widget) {
        Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
            @Override
            public void execute() {
                if (widget instanceof TextField) {
                    ((TextField) widget).focus();
                }
            }
        });
    }

    public SelectEvent.SelectHandler buttonSaveSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                if (!formPanel.isValid()) {
                    buttonSave.setEnabled(true);
                    return;
                }
                buttonSave.setEnabled(false);
            }
        };
    }

    public SelectEvent.SelectHandler buttonResetSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                formPanel.reset();
            }
        };
    }

    public KeyDownHandler buttonResetKeyDownHandler() {
        return new KeyDownHandler() {
            @Override
            public void onKeyDown(KeyDownEvent event) {
                if (event.getNativeKeyCode() == 9) {
                    windowFormView.focus();
                }
            }
        };
    }

}
