/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.custom;

import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FormPanel;

/**
 * @author krissadewo
 *         <p>Custom implementation for field label</p>
 */
public class CustomFieldLabel extends FieldLabel {

    /**
     * @param widget widget that will be implementated
     * @param label  name of label
     */
    public CustomFieldLabel(Widget widget, String label) {
        super(widget, label);
        this.setLabelAlign(FormPanel.LabelAlign.LEFT);
        this.setLabelSeparator(" :");
        this.setLabelWidth(120);
        if (label.isEmpty()) {
            this.setLabelSeparator("");
        }
    }

    /**
     * @param widget     widget that will be implementated
     * @param label      name of label
     * @param labelAlign label alignment
     */
    public CustomFieldLabel(Widget widget, String label, FormPanel.LabelAlign labelAlign) {
        super(widget, label);
        this.setLabelAlign(labelAlign);
        this.setLabelSeparator(" :");
        this.setLabelWidth(120);
        if (label.isEmpty()) {
            this.setLabelSeparator("");
        }
    }

    /**
     * @param label       name of label
     * @param labelHeight label height
     */
    public CustomFieldLabel(Widget widget, String label, int labelHeight) {
        super(widget, label);
        this.setLabelAlign(FormPanel.LabelAlign.LEFT);
        this.setLabelSeparator(" :");
        this.setLabelWidth(labelHeight);
        if (label.isEmpty()) {
            this.setLabelSeparator("");
        }
    }

}
