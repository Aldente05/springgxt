/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.properties;

import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor;

/**
 * @author krissadewo
 */
public class DoublePropertyEditor extends NumberPropertyEditor<Double> {

    public DoublePropertyEditor(Double incrAmount) {
        super(incrAmount);
    }

    @Override
    protected Double doDecr(Double value) {
        return value;
    }

    @Override
    protected Double doIncr(Double value) {
        return value;
    }

    @Override
    protected Double parseString(String string) {
        return Double.parseDouble(string);
    }

    @Override
    protected Double returnTypedValue(Number number) {
        return number.doubleValue();
    }

    @Override
    public String render(Double aDouble) {
        return null;
    }
}
