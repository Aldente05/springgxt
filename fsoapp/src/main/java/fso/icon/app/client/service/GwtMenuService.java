/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import fso.icon.core.entity.Menu;
import fso.icon.core.entity.User;

import java.util.ArrayList;

/**
 * @author krissadewo <dailycode.org>
 * @date Jul 18, 2013
 */
@RemoteServiceRelativePath("springGwtServices/gwtMenuService")
public interface GwtMenuService extends RemoteService {

    ArrayList<Menu> createTreeMenu();

    ArrayList<Menu> createTreeMenu(User role);
}
