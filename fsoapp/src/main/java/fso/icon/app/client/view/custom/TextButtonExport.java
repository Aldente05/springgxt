package fso.icon.app.client.view.custom;

import com.sencha.gxt.cell.core.client.ButtonCell;
import com.sencha.gxt.widget.core.client.button.TextButton;
import fso.icon.app.client.icon.Icon;

/**
 * Created with IntelliJ IDEA.
 * User: krissadewo
 * Date: 11/24/13
 * Time: 6:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class TextButtonExport extends TextButton {

    public TextButtonExport() {
        super("Export");
        this.setIcon(Icon.INSTANCE.exportExcel());
        this.setIconAlign(ButtonCell.IconAlign.RIGHT);
    }
}