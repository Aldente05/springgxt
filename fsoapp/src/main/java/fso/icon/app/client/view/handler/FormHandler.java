/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.handler;

import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import fso.icon.app.client.view.View;

/**
 *
 * @author krissadewo <dailycode.org>
 * @date Jul 7, 2013
 */
public interface FormHandler {

    /**
     * Show the specific view, that need to extend view
     *
     * @param view
     */
    void showForm(View view);

    SelectHandler buttonSaveSelectHandler();
}
