/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.properties;

import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import fso.icon.core.entity.Menu;

/**
 * @author krissadewo
 */
public interface MenuProperties extends PropertyAccess<Menu> {

    @Editor.Path("id")
    ModelKeyProvider<Menu> key();

    @Editor.Path("nama")
    ValueProvider<Menu, String> valueNamaMenu();

    @Editor.Path("userMenu.canRead")
    ValueProvider<Menu, Boolean> valueCanRead();

    @Editor.Path("userMenu.canSave")
    ValueProvider<Menu, Boolean> valueCanSave();

    @Editor.Path("userMenu.canEdit")
    ValueProvider<Menu, Boolean> valueCanEdit();

    @Editor.Path("userMenu.canDelete")
    ValueProvider<Menu, Boolean> valueCanDelete();

    @Editor.Path("userMenu.canPrint")
    ValueProvider<Menu, Boolean> valueCanPrint();
}
