/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.custom;

import com.sencha.gxt.cell.core.client.ButtonCell;
import com.sencha.gxt.widget.core.client.button.TextButton;
import fso.icon.app.client.icon.Icon;

/**
 *
 * @author krissadewo <dailycode.org>
 * @date Aug 14, 2013
 */
public class TextButtonDelete extends TextButton {

    public TextButtonDelete() {
        super("Hapus");
        this.setIcon(Icon.INSTANCE.delete());
        this.setIconAlign(ButtonCell.IconAlign.RIGHT);
    }
}
