/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.handler;

import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;

/**
 * @author krissadewo <dailycode.org>
 * @date Jul 5, 2013
 */
public interface GridHandler {

    SelectHandler buttonAddSelectHandler();

    SelectHandler buttonEditSelectHandler();

    SelectHandler buttonDeleteSelectHandler();

    SelectHandler buttonSearchSelectHandler();

    SelectHandler buttonPrintSelectHandler();
}
