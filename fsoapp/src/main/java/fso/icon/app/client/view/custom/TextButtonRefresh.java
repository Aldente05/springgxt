/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.custom;

import com.sencha.gxt.cell.core.client.ButtonCell;
import com.sencha.gxt.widget.core.client.button.TextButton;
import fso.icon.app.client.icon.Icon;

/**
 *
 * @author krissadewo <dailycode.org>
 * @date Jul 15, 2013
 */
public class TextButtonRefresh extends TextButton {

    public TextButtonRefresh() {
        super("Refresh");
        this.setIcon(Icon.INSTANCE.refresh());
        this.setIconAlign(ButtonCell.IconAlign.RIGHT);
    }
}
