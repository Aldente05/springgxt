package fso.icon.app.client.view.custom;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import fso.icon.core.AppCore;
import com.sencha.gxt.core.client.util.DateWrapper;
import com.sencha.gxt.widget.core.client.form.DateField;
import com.sencha.gxt.widget.core.client.form.DateTimePropertyEditor;
import com.sencha.gxt.widget.core.client.form.validator.MaxDateValidator;
import com.sencha.gxt.widget.core.client.form.validator.MinDateValidator;

import java.util.Date;

/**
 * @author krissadewo
 */
public class DateFieldCustom extends DateField {

    public DateFieldCustom() {
        this.setEditable(false);
        this.setPropertyEditor(new DateTimePropertyEditor(AppCore.STANDARD_DATE_FORMAT));
        this.addValidator(new MinDateValidator(new DateWrapper().getFirstDayOfMonth().asDate()));
        this.addValidator(new MaxDateValidator(new DateWrapper().asDate()));
        this.setValue(new DateWrapper().asDate());

        this.getDatePicker().addValueChangeHandler(new ValueChangeHandler<Date>() {
            @Override
            public void onValueChange(ValueChangeEvent<Date> event) {
                setValue(event.getValue());
            }
        });
    }
}
