/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.properties;

import com.google.gwt.editor.client.Editor.Path;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import fso.icon.core.entity.Month;

/**
 * @author Tejo Baskoro
 */
public interface MonthProperties extends PropertyAccess<Month> {

    @Path("month")
    ModelKeyProvider<Month> key();

    @Path("month")
    ValueProvider<Month, Integer> valueMonth();

    @Path("monthName")
    ValueProvider<Month, String> valueMonthName();

    @Path("monthName")
    LabelProvider<Month> labelMonthName();
}
