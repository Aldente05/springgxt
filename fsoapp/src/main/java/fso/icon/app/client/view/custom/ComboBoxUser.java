/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.custom;

import com.google.gwt.user.client.ui.Widget;
import fso.icon.app.client.view.View;
import fso.icon.core.entity.User;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.form.ComboBox;

/**
 * @author krissadewo
 */
public class ComboBoxUser extends View {

    private ComboBox<User> comboBox;
    private ListStore<User> listStore;
    private boolean isSetSelectedValue;

    public ComboBoxUser() {
        initData();
    }

    public ComboBoxUser(boolean isSetSelectedValue) {
        this.isSetSelectedValue = isSetSelectedValue;
        initData();
    }

    @Override
    public Widget asWidget() {
        listStore = new ListStore<User>(getProperties().getUserProperties().key());

        comboBox = new ComboBox<User>(listStore, getProperties().getUserProperties().labelRealname());
        comboBox.setEmptyText("Pilih Jurusan");
        comboBox.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
        comboBox.setEditable(true);
        return comboBox;
    }

    private void initData() {
//        getService().getUserServiceAsync().findAll(new AsyncCallback<ArrayList<User>>() {
//            @Override
//            public void onFailure(Throwable caught) {
//                AppClient.showMessageOnFailureException(caught);
//            }
//
//            @Override
//            public void onSuccess(ArrayList<User> result) {
//                listStore.replaceAll(result);
//                if (isSetSelectedValue) {
//                    if (!result.isEmpty())
//                        comboBox.setValue(result.get(0));
//                }
//            }
//        });
    }

}
