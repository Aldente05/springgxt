/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.properties;

import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor;

/**
 * @author krissadewo
 */
public class IntegerPropertyEditor extends NumberPropertyEditor<Integer> {

    public IntegerPropertyEditor(Integer incrAmount) {
        super(incrAmount);
    }

    @Override
    protected Integer doDecr(Integer value) {
        return value;
    }

    @Override
    protected Integer doIncr(Integer value) {
        return value;
    }

    @Override
    protected Integer parseString(String string) {
        return Integer.parseInt(string);
    }

    @Override
    protected Integer returnTypedValue(Number number) {
        return number.intValue();
    }

    @Override
    public String render(Integer integer) {
        return null;
    }
}
