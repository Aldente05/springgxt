/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 *
 * @author krissadewo <dailycode.org>
 * @date Jul 5, 2013
 */
public interface GwtSessionHandlerServiceAsync {

    void isValidSession(AsyncCallback<Boolean> callback);

    void getUserSessionTimeout(AsyncCallback<Integer> callback);

    void isSessionAlive(AsyncCallback<Boolean> callback);
}
