package fso.icon.app.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface GwtHomeServiceAsync {

    void getAppTitle(AsyncCallback<String> callback);
}
