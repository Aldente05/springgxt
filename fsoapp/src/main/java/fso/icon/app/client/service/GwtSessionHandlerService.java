/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 *
 * @author krissadewo <dailycode.org>
 * @date Jul 5, 2013
 */
@RemoteServiceRelativePath("springGwtServices/gwtSessionHandlerService")
public interface GwtSessionHandlerService extends RemoteService {

    boolean isValidSession();

    boolean isSessionAlive();

    Integer getUserSessionTimeout();
}
