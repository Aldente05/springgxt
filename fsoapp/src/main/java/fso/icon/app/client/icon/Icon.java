/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.icon;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * @author Tejo Baskoro
 */
public interface Icon extends ClientBundle {

    public Icon INSTANCE = GWT.create(Icon.class);

    @Source("content.gif")
    ImageResource menuIcon();

    @Source("sync.png")
    ImageResource sync();

    @Source("add.gif")
    ImageResource add();

    @Source("export.png")
    ImageResource export();

    @Source("export_excel.png")
    ImageResource exportExcel();

    @Source("save.png")
    ImageResource save();

    @Source("money.png")
    ImageResource money();

    @Source("edit.png")
    ImageResource edit();

    @Source("printer.png")
    ImageResource print();

    @Source("upload.png")
    ImageResource upload();

    @Source("delete.gif")
    ImageResource delete();

    @Source("logout.png")
    ImageResource logout();

    @Source("loading.gif")
    ImageResource loading();

    @Source("refresh.png")
    ImageResource refresh();

    @Source("cancel.png")
    ImageResource cancel();

    @Source("reset.png")
    ImageResource reset();

    @Source("search.png")
    ImageResource search();

    @Source("grid.png")
    ImageResource grid();

    @Source("info.png")
    ImageResource info();

    @Source("user.png")
    ImageResource user();
}
