package fso.icon.app.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.sencha.gxt.core.client.GXT;
import com.sencha.gxt.state.client.CookieProvider;
import com.sencha.gxt.state.client.StateManager;
import fso.icon.app.client.view.View;
import fso.icon.app.client.view.HomeView;

/**
 * Created by f.putra on 7/31/17.
 */
public class AppModule implements EntryPoint {

    @Override
    public void onModuleLoad() {
        StateManager.get().setProvider(new CookieProvider("/", null, null, GXT.isSecure()));
        checkAuthenticatedUser();
        RootPanel.get().clear();
    }

    private void checkAuthenticatedUser() {
//        GwtBaseService.getInstance().getUserServiceAsync().isAuthenticatedUser(new AsyncCallback<Boolean>() {
//            @Override
//            public void onFailure(Throwable caught) {
//                AppClient.showMessageOnFailureException(caught);
//            }
//
//            @Override
//            public void onSuccess(Boolean result) {
//                if (result) {
                    RootPanel.get().add(View.loadView(new HomeView()));
//                } else {
//                    RootPanel.get().add(View.loadView(new LoginView()));
//                }
//            }
//        });
    }
}
