/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.properties;

import com.google.gwt.core.client.GWT;

/**
 * @author krissadewo
 *         <p/>
 *         Base class to handle properties for view, register all properties here to
 *         serve our view
 */
public class BaseProperties {

    private static BaseProperties instance;
    private final MenuProperties menuProperties = GWT.create(MenuProperties.class);
    private final MonthProperties monthProperties = GWT.create(MonthProperties.class);
    private final UserProperties userProperties = GWT.create(UserProperties.class);
    private final CustomFieldProperties customFieldProperties = GWT.create(CustomFieldProperties.class);

    private BaseProperties() {
    }

    public static BaseProperties getInstance() {
        if (instance == null) {
            instance = new BaseProperties();
        }
        return instance;
    }

    public CustomFieldProperties getCustomFieldProperties() {
        return customFieldProperties;
    }

    public MenuProperties getMenuProperties() {
        return menuProperties;
    }

    public MonthProperties getMonthProperties() {
        return monthProperties;
    }


    public UserProperties getUserProperties() {
        return userProperties;
    }
}
