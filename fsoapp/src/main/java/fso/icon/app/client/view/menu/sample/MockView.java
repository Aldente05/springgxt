/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.client.view.menu.sample;

import com.google.gwt.user.client.ui.Widget;
import fso.icon.app.client.view.View;
import fso.icon.app.client.view.custom.GridView;
import fso.icon.core.entity.Menu;
import com.sencha.gxt.core.client.resources.ThemeStyles;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.toolbar.ToolBar;

/**
 * @author krissadewo
 */
public class MockView extends View {

    public MockView(Menu menu) {
        super(menu);
    }

    @Override
    public Widget asWidget() {
//        RpcProxy<PagingLoadConfig, PagingLoadResult<Mock>> proxy = new RpcProxy<PagingLoadConfig, PagingLoadResult<Mock>>() {
//            @Override
//            public void load(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<Mock>> callback) {
//                getService().getGwtMockServiceAsync().findAll(loadConfig, callback);
//            }
//        };
//
//        final ListStore listStore = new ListStore<Mock>(getProperties().getMockProperties().key());
//
//        final PagingLoader<PagingLoadConfig, PagingLoadResult<Mock>> gridLoader = new PagingLoader<PagingLoadConfig, PagingLoadResult<Mock>>(proxy);
//        gridLoader.setRemoteSort(true);
//
//        ColumnConfig<Mock, String> forumColumn = new ColumnConfig<Mock, String>(getProperties().getMockProperties().valueFirstName(), 150, "First Name");
//        ColumnConfig<Mock, String> usernameColumn = new ColumnConfig<Mock, String>(getProperties().getMockProperties().valueLastName(), 150, "Last Name");
//        ColumnConfig<Mock, Integer> subjectColumn = new ColumnConfig<Mock, Integer>(getProperties().getMockProperties().valueAge(), 50, "Age");
//
//        List<ColumnConfig<Mock, ?>> l = new ArrayList<ColumnConfig<Mock, ?>>();
//        l.add(forumColumn);
//        l.add(usernameColumn);
//        l.add(subjectColumn);
//
//        ColumnModel<Mock> cm = new ColumnModel<Mock>(l);
//
//        final LiveGridView<Mock> liveGridView = new LiveGridView<Mock>();
//        liveGridView.setForceFit(true);
//
//        Grid<Mock> view = new Grid<Mock>(listStore, cm) {
//            @Override
//            protected void onAfterFirstAttach() {
//                super.onAfterFirstAttach();
//                Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
//                    @Override
//                    public void execute() {
//                        gridLoader.load(0, liveGridView.getCacheSize());
//                    }
//                });
//            }
//        };

//        view.setLoadMask(true);
//        view.setLoader(gridLoader);
//        view.setView(liveGridView);

        ToolBar toolBar = new ToolBar();
//        toolBar.add(new LiveToolItem(view));
        toolBar.addStyleName(ThemeStyles.get().style().borderTop());
        toolBar.getElement().getStyle().setProperty("borderBottom", "none");

        GridView gridView = new GridView();
        gridView.setHeaderVisible(false);
//        gridView.addWidget(view, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        gridView.addWidget(toolBar, new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        return gridView;
    }
}
