/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.server.service;

import fso.icon.app.client.service.GwtMenuService;
import fso.icon.core.AppCore;
import fso.icon.core.entity.Menu;
import fso.icon.core.entity.User;
import fso.icon.core.service.NavigationMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * @author krissadewo
 */
@Service("gwtMenuService")
public class MenuServiceImpl implements GwtMenuService {

    @Autowired
    private NavigationMenuService service;

    @Override
    public ArrayList<Menu> createTreeMenu(User role) {
        return new ArrayList<Menu>(service.createTreeMenu(role));
    }

    @Override
    public ArrayList<Menu> createTreeMenu() {
        return new ArrayList<Menu>(service.createTreeMenu(AppCore.getInstance().getUserFromSession()));
    }

}
