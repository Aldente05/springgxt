package fso.icon.app.server.service;

import fso.icon.app.client.service.GwtHomeService;
import fso.icon.core.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by krissadewo on 3/10/14.
 */
@Service("gwtHomeService")
public class HomeServiceImpl implements GwtHomeService {

    @Autowired(required = false)
    private Profile profile;

    @Override
    public String getAppTitle() {
        return profile.getAppTitle();
    }
}
