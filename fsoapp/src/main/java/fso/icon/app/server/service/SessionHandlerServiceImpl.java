/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fso.icon.app.server.service;

import fso.icon.core.AppCore;
import fso.icon.app.client.service.GwtSessionHandlerService;
import org.springframework.stereotype.Service;

/**
 * @author krissadewo <dailycode.org>
 * @date Jul 5, 2013
 */
@Service("gwtSessionHandlerService")
public class SessionHandlerServiceImpl implements GwtSessionHandlerService {

    private int timeout;

    @Override
    public boolean isValidSession() {
        if (!AppCore.getInstance().getHttpServletRequest().isRequestedSessionIdValid()) {
            AppCore.getLogger(this).info("session expired !!! ");
            return false;
        }

        return true;
    }

    @Override
    @Deprecated
    public boolean isSessionAlive() {
        return (System.currentTimeMillis() - AppCore.getInstance().getHttpServletRequest().getSession().getLastAccessedTime()) < timeout;
    }

    @Override
    public Integer getUserSessionTimeout() {
        timeout = AppCore.getInstance().getHttpServletRequest().getSession().getMaxInactiveInterval() * 1000;
        return timeout;
    }
}
