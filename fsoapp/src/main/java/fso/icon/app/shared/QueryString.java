package fso.icon.app.shared;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: krissadewo
 * Date: 11/24/13
 * Time: 5:02 PM
 * To change this template use File | Settings | File Templates.
 * This class provide a function to create query string without repeating writing hard code when calling something in url
 */
public class QueryString {

    private static StringBuilder query;

    public static QueryString getInstance() {
        query = new StringBuilder("?");
        return QueryStringHolder.INSTANCE;
    }

    public QueryString addQueryBulan(int value) {
        query.append("bulan=").append(value).append("&");
        return this;
    }

    public QueryString addQueryTahun(int value) {
        query.append("tahun=").append(value).append("&");
        return this;
    }

    public QueryString addQueryTglAwal(Date value) {
        query.append("tglAwal=").append(value.getTime()).append("&");
        return this;
    }

    public QueryString addQueryTglAkhir(Date value) {
        query.append("tglAkhir=").append(value.getTime()).append("&");
        return this;
    }

    public QueryString addQueryShowAll(Boolean value) {
        query.append("showAll=").append(value).append("&");
        return this;
    }

    public QueryString addQueryStatusPenjualanLunas(Boolean value) {
        query.append("statusPenjualanLunas=").append(value).append("&");
        return this;
    }

    public QueryString addQueryStatusPenjualanUpload(Boolean value) {
        query.append("statusPenjualanUpload=").append(value).append("&");
        return this;
    }

    public QueryString addQueryType(String value) {
        query.append("type=").append(value).append("&");
        return this;
    }

    public QueryString addQueryStatus(String value) {
        query.append("status=").append(value).append("&");
        return this;
    }

    public QueryString addQueryJenisTanggal(String value) {
        query.append("jenisTanggal=").append(value).append("&");
        return this;
    }

    public QueryString addQueryJenisPencarian(String value) {
        query.append("jenisPencarian=").append(value).append("&");
        return this;
    }

    public QueryString addQueryKodePelanggan(String value) {
        query.append("kodePelanggan=").append(value).append("&");
        return this;
    }

    public QueryString addQueryNamaPelanggan(String value) {
        query.append("namaPelanggan=").append(value).append("&");
        return this;
    }

    public QueryString addQueryIdKategoriPelanggan(Long value) {
        query.append("kategoriPelanggan=").append(value).append("&");
        return this;
    }

    public QueryString addQueryKodeRekanan(String value) {
        query.append("kodeRekanan=").append(value).append("&");
        return this;
    }

    public QueryString addQueryKodeAkun(String value) {
        query.append("kodeAkun=").append(value).append("&");
        return this;
    }

    public QueryString addQueryNamaRekanan(String value) {
        query.append("namaRekanan=").append(value).append("&");
        return this;
    }

    public QueryString addQueryNamaReferensi(String value) {
        query.append("namaReferensi=").append(value).append("&");
        return this;
    }

    public QueryString addQueryNamaTransaksi(String value) {
        query.append("namaTransaksi=").append(value).append("&");
        return this;
    }

    public QueryString addQueryIdKategoriRekanan(Long value) {
        query.append("kategoriRekanan=").append(value).append("&");
        return this;
    }

    public QueryString addQueryValuePencarian(String value) {
        query.append("valuePencarian=").append(value).append("&");
        return this;
    }

    public QueryString addQueryIdJenisTransaksi(Long value) {
        query.append("idJenisTransaksi=").append(value).append("&");
        return this;
    }

    public QueryString addQueryIdPelanggan(Long value) {
        query.append("idPelanggan=").append(value).append("&");
        return this;
    }

    public QueryString addQueryIdRekanan(Long value) {
        query.append("idRekanan=").append(value).append("&");
        return this;
    }

    public QueryString addQueryIdAkunKredit(Long value) {
        query.append("idAkunKredit=").append(value).append("&");
        return this;
    }

    public QueryString addQueryIdAkunDebet(Long value) {
        query.append("idAkunDebet=").append(value).append("&");
        return this;
    }

    public QueryString addQueryIdAkun(Long value) {
        query.append("idAkun=").append(value).append("&");
        return this;
    }

    public QueryString addQueryIdUser(Long value) {
        query.append("idUser=").append(value).append("&");
        return this;
    }

    public QueryString addQueryIdMataUang(Long value) {
        query.append("idMataUang=").append(value).append("&");
        return this;
    }

    public QueryString addQueryKeterangan(String value) {
        query.append("keterangan=").append(value).append("&");
        return this;
    }

    public QueryString addQueryNoBukti(String value) {
        query.append("noBukti=").append(value).append("&");
        return this;
    }

    public QueryString addQueryPengajuan(String value) {
        query.append("pengajuan=").append(value).append("&");
        return this;
    }

    public QueryString addQueryKodeBooking(String value) {
        query.append("kodeBooking=").append(value).append("&");
        return this;
    }

    public QueryString addQueryNoTiket(String value) {
        query.append("noTiket=").append(value).append("&");
        return this;
    }

    public QueryString addQueryAllRekanan(String value) {
        query.append("allRekanan=").append(value).append("&");
        return this;
    }

    public QueryString addQueryPerRekanan(String value) {
        query.append("perRekanan=").append(value).append("&");
        return this;
    }

    public QueryString addQueryAllAgen(String value) {
        query.append("allRekanan=").append(value).append("&");
        return this;
    }

    public QueryString addQueryPerAgen(String value) {
        query.append("perRekanan=").append(value).append("&");
        return this;
    }

    public QueryString addQueryPerson(String value) {
        query.append("person=").append(value).append("&");
        return this;
    }

    public QueryString addQueryPengeluaran(String value) {
        query.append("pengeluaran=").append(value).append("&");
        return this;
    }

    public QueryString addQueryJenisBayar(String value) {
        query.append("jenisBayar=").append(value).append("&");
        return this;
    }

    public QueryString build() {
        query.replace(query.length() - 1, query.length(), "");
        return this;
    }

    public StringBuilder getQuery() {
        return query;
    }


    private static class QueryStringHolder {

        private static final QueryString INSTANCE = new QueryString();
    }
}
