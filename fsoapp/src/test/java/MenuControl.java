import fso.icon.core.entity.Menu;
import fso.icon.core.entity.User;
import fso.icon.core.service.NavigationMenuService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

/**
 * Created by f.putra on 7/21/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:application-context.xml"})
@WebAppConfiguration
public class MenuControl {
    @Autowired
    private NavigationMenuService navigationMenuService;
    private User user;

    @Before
    public void init() {
        user = new User();
        user.setEmail("54.ADMIN");
    }

    @Test
    public void MenuList() {
        List<Menu> menuList = navigationMenuService.menuByUser(user);
        for (Menu item : menuList) {
            System.out.println(item.getTitle());
        }
    }
}
